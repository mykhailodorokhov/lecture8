import { Component, Output, EventEmitter } from "@angular/core";
import { Query } from "app/sales/definitions";

@Component({
    selector: 'query-form',
    templateUrl: './query-form.component.html'
    /*template: `
    <input type="text" [(ngModel)]="query.name">
    <input type="date" [(ngModel)]="query.startDate">
    <input type="date" [(ngModel)]="query.endDate">

    <button (click)="executeEventQuery.emit(query)">Query plant catalog</button>
    
    {{query|json}}
    `*/
})
export class QueryFormComponent {
    @Output() executeEventQuery: EventEmitter<Query> = new EventEmitter();

    query: Query = new Query();
}